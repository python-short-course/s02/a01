def is_leap_year() -> None:

    while True:
        try:   
            user_input: int = int(input("Enter a valid year: \n"))
            if user_input <= 0:
                raise ValueError
            break
         
        except ValueError:
            print("Only positive integers are allowed, except zero.")

    if user_input % 4 == 0:
        print(f"{user_input} is a leap year.")
    else:
        print(f"{user_input} is not a leap year.")

is_leap_year()

###############################################################

col_input: int = int(input("Enter number of columns: \n"))
row_input: int = int(input("Enter number of rows: \n"))

def make_grid(cols: int, rows: int) -> None:
    for _ in range(rows):
        print("*" * cols)
            
make_grid(col_input, row_input)