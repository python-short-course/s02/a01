# username: str = input("Please Enter Your Name: \n")
# print(f"Hello, {username}")

# num1: int = int(input("Enter a number:"))
# num2: int = int(input("Enter a number:"))

# print(num1 + num2)

num: int = int(input("Enter Number: "))
if num % 3  == 0 and num % 5 == 0:
    print(f"{num} is divisible by 3 and 5")
elif num % 3 == 0:
    print(f"{num} is divisible by 3")
elif num % 5 == 0:
    print(f"{num} is divisible by 5")
else:
    print(f"{num} is not divisible by 3 nor 5")